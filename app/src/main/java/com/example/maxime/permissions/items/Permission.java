package com.example.maxime.permissions.items;

/**
 * Created by maxime on 08/10/2017.
 */

public class Permission {

    private String label;
    private boolean authorized;

    public Permission(String label, boolean authorized) {
        this.label = label;
        this.authorized = authorized;
        System.out.println("Permission created");
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isAuthorized() {
        return authorized;
    }

    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }
}
