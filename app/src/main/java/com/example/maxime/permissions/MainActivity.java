package com.example.maxime.permissions;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.maxime.permissions.items.Permission;
import com.example.maxime.permissions.items.PermissionAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final AppCompatActivity _this = this;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    private static final int MY_PERMISSIONS_REQUEST_READ_CALENDAR = 2;
    private static final int MY_PERMISSIONS_REQUEST_READ_SMS = 3;
    private static final int MY_PERMISSION_REQUEST_STORAGE_GROUP = 4;
    private final String[] storage_permissions = {};

    List<Permission> permissions = null;
    PermissionAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listPermissions();

        ListView mListView = (ListView) findViewById(R.id.listView);

        adapter = new PermissionAdapter(MainActivity.this, permissions);

        mListView.setAdapter(adapter);


        // Petit listener de clicks dans une liste
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) { switch(position) {
                    case 0:
                        //* Remplacer false par une vérification pour savoir si la lecture du calendrier est déjà autorisée.
                        if (false) {
                            //* Demander permission de lecture du calendrier
                        }
                        break;
                    case 1:
                        //* Remplacer false par une vérification pour savoir si la lecture des SMS est déjà autorisé
                        if (false) {
                            //* Demander permission de lecture des SMS
                        }
                        break;
                    case 2:
                        //* Remplacer false par un check pour savoir si les contacts sont déjà autorisés
                        if (false) {
                            showDescriptionDialog("Ceci est un exemple d'explication de ce pourquoi une autorisation est nécessaire",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    //* Demander permission de lecture des contacts
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    Toast.makeText(_this, "Annulation demande de permission", Toast.LENGTH_SHORT).show();
                                                    break;
                                            }
                                        }
                                    });
                        }
                        break;
                    case 3 :
                        //* Remplacer false par une permission groupée d'appels et de stockage
                        if (false) {
                            //* Demander permissions d'appels et de stockage
                        }
                        break;
                    default:
                        Toast.makeText(getApplicationContext(), "Contente-toi des cas gérés !", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    /**
     * Permet de gérer l'acceptation ou le refus d'une permission (fait office de callback)
     * @param requestCode Code permettant de savoir quelle permission est demandée (à définir par l'utilisateur)
     * @param permissions Liste des permissions qui ont été demandés
     * @param grantResults Résultat de la demande de permission (acceptation ou refus)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        //* Complétez cette classe
    }

    /**
     * Permet de générer la liste des permissions qui seront afficher à l'écran
     */
    public void listPermissions() {
        permissions = new ArrayList<>();
        addPermissions();
    }

    /**
     * Permet de mettre à jour l'affichage de l'état d'une permission (acceptée ou refusée)
     */
    private void refreshPermissions() {
        permissions.clear();
        addPermissions();
        adapter.notifyDataSetChanged();
    }


    /**
     * Simplification pour l'affichage d'une boite de dialogue
     * @param message Le message à afficher
     * @param okListener Le listener permettant de savoir quelle action a été choisie ('OK' ou 'annuler')
     */
    private void showDescriptionDialog(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    /**
     * Permet de vérifier un group de permissions
     * Ici il s'agit des permissions de stockage et d'appel qui sont demandées en même temps.
     * @param context Activité actuelle
     * @param permissions Liste des permissions à vérifier
     * @return True si l'ensemble est vérifié, false sinon
     */
    public static boolean hasStorageAndCallsPermissions(Context context, String... permissions) {
        if(permissions.length == 0) return false;
        for (String permission : permissions) {
            //* Remplacer false par une vérification de toutes les permissions envoyées en paramètre
            if (false) {
                return false;
            }
        }
        return true;
    }




































































































    /**
     * Ajoute toutes les permissions nécessaires avec une vérification de leur état
     */
    private void addPermissions() {
        permissions.add(new Permission("Lire calendrier", ContextCompat.checkSelfPermission(_this, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED));
        permissions.add(new Permission("Lire des SMS", ContextCompat.checkSelfPermission(_this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED));
        permissions.add(new Permission("Lire contacts (Avec description avant)", ContextCompat.checkSelfPermission(_this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED));
        permissions.add(new Permission("Permissions de stockage et d'appel", hasStorageAndCallsPermissions(_this, storage_permissions)));
    }
}
