package com.example.maxime.permissions.items;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.maxime.permissions.R;

import java.util.List;

public class PermissionAdapter extends ArrayAdapter<Permission> {

    public PermissionAdapter(Context context, List<Permission> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        System.out.println("GetView");

        if(convertView == null) {

            System.out.println("ConvertView null");

//            convertView = LayoutInflater.from(getContext()).inflate(R.layout.permission_list, parent);
//            convertView = LayoutInflater.from(getContext()).inflate(R.layout.permission_list, parent);
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.permission_list, parent, false);
        }

        PermissionViewHolder viewHolder = (PermissionViewHolder) convertView.getTag();

        if(viewHolder == null) {

            System.out.println("ViewHolder null");

            viewHolder = new PermissionViewHolder();
            viewHolder.label = (TextView) convertView.findViewById(R.id.label);
            viewHolder.permission_color = (ImageView) convertView.findViewById(R.id.permission_color);
            convertView.setTag(viewHolder);
        }

        Permission permission = getItem(position);

        viewHolder.label.setText(permission.getLabel());
        viewHolder.permission_color.setImageDrawable(new ColorDrawable(permission.isAuthorized() ? Color.parseColor("#00FF88") : Color.parseColor("#FF4444")));

        System.out.println("ViewHolder done");

        return convertView;
    }


    private class PermissionViewHolder {
        public TextView label;
        public ImageView permission_color;
    }
}
